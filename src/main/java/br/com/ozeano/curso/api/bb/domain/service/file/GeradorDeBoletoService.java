package br.com.ozeano.curso.api.bb.domain.service.file;

public interface GeradorDeBoletoService {

	public byte[] gerar();
	
}
